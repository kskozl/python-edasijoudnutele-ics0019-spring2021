import cartopy
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import csv


class Airport:
    def __init__(self, name, city, country, IATA, latitude, longitude):
        self.longitude = longitude
        self.latitude = latitude
        self.IATA = IATA
        self.country = country
        self.city = city
        self.name = name

    def __repr__(self):
        return f"Airport {self.name} in {self.city}, {self.country}, with IATA: {self.IATA}"


def collect_flights_to_list(flights_path, airports_path) -> list:
    with open(flights_path, "r", encoding="UTF-8") as csv_file, \
            open(airports_path, "r", encoding="UTF-8") as dat_file:
        flights_reader = csv.reader(csv_file, delimiter=";")
        data_dict = {}
        dat_file.readline()
        for line in dat_file.readlines():
            line = line.split(",")
            name = line[1].replace('"', '')
            city = line[2].replace('"', '')
            country = line[3].replace('"', '')
            IATA = line[4].replace('"', '')
            latitude = line[6]
            longitude = line[7]
            data_dict[IATA] = Airport(name, city, country, IATA, latitude, longitude)

        next(flights_reader)

        airports_data = []
        for line in flights_reader:
            city, IATA = line
            airports_data.append(data_dict.get(IATA))

        return airports_data


def add_flight(start_airport: Airport, finish_airport: Airport, color):
    start_longitude = float(start_airport.longitude)
    start_latitude = float(start_airport.latitude)
    finish_longitude = float(finish_airport.longitude)
    finish_latitude = float(finish_airport.latitude)

    plt.plot([start_longitude, finish_longitude], [start_latitude, finish_latitude],
             color=color, linewidth=0.5, marker='o',
             transform=ccrs.Geodetic())


def add_airport_name(airport: Airport):
    longitude = float(airport.longitude)
    latitude = float(airport.latitude)

    plt.text(longitude, latitude + 0.25, airport.IATA,
             horizontalalignment='right',
             transform=ccrs.PlateCarree())


if __name__ == '__main__':
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.add_feature(cartopy.feature.LAND)
    ax.add_feature(cartopy.feature.OCEAN)
    ax.add_feature(cartopy.feature.LAKES)
    ax.add_feature(cartopy.feature.COASTLINE)
    ax.add_feature(cartopy.feature.BORDERS, linestyle='-', alpha=.5)
    plt.title("HW3, Ksenija Kozlova")

    airports_usual_time = collect_flights_to_list("data/otselennud.csv", "data/airports.dat")
    usual_point = airports_usual_time[0]
    add_airport_name(usual_point)

    for airport in airports_usual_time:
       add_flight(airport, usual_point, "green")
       add_airport_name(airport)

    airports_covid_time = collect_flights_to_list("data/flights21.csv", "data/airports.dat")
    corona_point = airports_covid_time[0]

    for airport in airports_covid_time:
       add_flight(airport, corona_point, "purple")

    plt.show()
    plt.savefig('flights.pdf')
