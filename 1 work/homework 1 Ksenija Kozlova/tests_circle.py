import unittest
import circle_kskozl.circle as circle
import math
import random

def fun_for_negative_num(wrong_radius):
    circle.circle_area_circumference(wrong_radius)

class MyTestCase(unittest.TestCase):

    def test_num(self):
        assert circle.circle_area_circumference(0)[0] == 0
        assert circle.circle_area_circumference(0)[1] == 0

        assert circle.circle_area_circumference(1)[0] == 6.283185307179586
        assert circle.circle_area_circumference(1)[1] == math.pi

        assert circle.circle_area_circumference(5)[0] == 31.41592653589793
        assert circle.circle_area_circumference(5)[1] == 78.53981633974483

        assert circle.circle_area_circumference(10)[0] == 62.83185307179586
        assert circle.circle_area_circumference(10)[1] == 314.1592653589793

    def test_float_num(self):
        assert circle.circle_area_circumference(1.5)[0] == 9.42477796076938
        assert circle.circle_area_circumference(1.5)[1] == 7.0685834705770345

        assert circle.circle_area_circumference(2.5)[0] == 15.707963267948966
        assert circle.circle_area_circumference(2.5)[1] == 19.634954084936208

    def test_random(self):
        ran = random.random()
        assert circle.circle_area_circumference(ran)[0] == 2 * math.pi * ran
        assert circle.circle_area_circumference(ran)[1] == math.pi * ran * ran

    def test_negative_num(self):
        self.assertRaises(RuntimeError, lambda: fun_for_negative_num(-1))
        self.assertRaises(RuntimeError, lambda: fun_for_negative_num(-15))
        self.assertRaises(RuntimeError, lambda: fun_for_negative_num(-3.5))


if __name__ == '__main__':
    unittest.main()

