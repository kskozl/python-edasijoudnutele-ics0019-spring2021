import circle_kskozl.circle

radius = float (input("Insert radius: "))

circumference, area = circle_kskozl.circle.circle_area_circumference(radius)
result = f"C = {circumference}, S = {area}"
print(result)
