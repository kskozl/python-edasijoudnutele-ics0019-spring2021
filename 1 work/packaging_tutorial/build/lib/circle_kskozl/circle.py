import math

def circle_area_circumference(radius):
    if radius >= 0:
        circumference = 2 * math.pi * radius
        area = math.pi * radius * radius
        return circumference, area
    else:
        raise RuntimeError()
