import scrapy

class Buy(scrapy.Spider):
    name = "buying_computer"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0'
    }

    def start_requests(self):
        url = "https://www.osta.ee/kategooria/arvutid/lauaarvutid"
        yield scrapy.http.Request(url, headers=self.headers)

    def parse(self, response):
        main_selector = '.main-content__section-helper'
        container = response.css(main_selector)

        thumb_selector = '.offer-thumb.offer-thumb__fancy'
        for thumb_fancy in container.css(thumb_selector):
            title_selector = 'h3 ::attr(title)'
            price_selector = '.offer-thumb__price--current ::text'
            image_selector = 'img ::attr(data-original)'

            information = {
                'name': thumb_fancy.css(title_selector).extract_first(),
                'price': thumb_fancy.css(price_selector).extract_first().strip(),
                'image': thumb_fancy.css(image_selector).extract_first()
            }
            yield information

        next_page_selector = './/a[span[@id="nextPage"]]/@href'
        next_page = response.xpath(next_page_selector).extract_first()

        if next_page:
            url = response.urljoin("https://www.osta.ee/" + next_page)
            yield scrapy.Request(url, self.parse, headers=self.headers)
