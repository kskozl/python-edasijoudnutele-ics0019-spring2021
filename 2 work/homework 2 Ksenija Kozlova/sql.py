import sqlite3


def open_database():
    """Open SQLite database."""
    global conn
    conn = sqlite3.connect('DINERS.db')
    conn.execute("DROP TABLE PROVIDER")
    conn.execute("DROP TABLE CANTEEN")
    print("Opened database successfully")


def create_table():
    # create a table in the previously created database
    conn.execute('''CREATE TABLE PROVIDER 
             (ID                INT PRIMARY KEY     NOT NULL,
             ProviderName       TEXT                NOT NULL)
             ''')
    print("Table PROVIDER created successfully")

    # create a table in the previously created database
    conn.execute('''CREATE TABLE CANTEEN 
             (ID            INT PRIMARY KEY    NOT NULL,
             ProviderID     INT                NOT NULL,
             NAME           TEXT               NOT NULL,
             LOCATION       VARCHAR(50)        NOT NULL,
             time_open      INTEGER               NOT NULL,
             time_closed    INTEGER               NOT NULL,
             FOREIGN KEY (ProviderID) REFERENCES PROVIDER (ID))
             ''')
    print("Table CANTEEN created successfully")


def create_records():
    """Create some records in the PROVIDER table. Create some records in the CANTEEN table"""
    conn.execute("""INSERT INTO PROVIDER (ID, ProviderName)
                 VALUES (1, 'Rahva Toit'),
                        (2, 'Baltic Restaurants Estonia AS'),
                        (3, 'TTÜ Sport'),
                        (4, 'Bitstop Kohvik OÜ')
                        """)


    conn.execute("""INSERT INTO CANTEEN (ID, ProviderID, NAME, LOCATION, time_open, time_closed)
                 VALUES (1, 1, 'Economics- and social science building canteen', 'Akadeemia tee 3, SOC- building', strftime('%H:%M', '08:30'), strftime('%H:%M', '18:30')),
                        (2, 1, 'Library canteen', 'Akadeemia tee 3, SOC- building', strftime('%H:%M', '08:30'), strftime('%H:%M', '19:00')),
                        (3, 2, 'Main building Deli cafe', 'Akadeemia tee 1/Ehitajate tee 7', strftime('%H:%M', '09:00'), strftime('%H:%M', '16:30')),
                        (4, 2, 'Main building Daily lunch restaurant', 'Ehitajate tee 5, U01 building', strftime('%H:%M', '09:00'), strftime('%H:%M', '16:30')),
                        (5, 1, 'U06 building canteen', ' ', strftime('%H:%M', '09:00'), strftime('%H:%M', '16:00')),
                        (6, 2, 'Natural Science building canteen', 'Akadeemia tee 15, SCI building', strftime('%H:%M', '09:00'), strftime('%H:%M', '16:00')),
                        (7, 2, 'ICT building canteen', 'Raja 15/Mäepealse 1', strftime('%H:%M', '09:00'), strftime('%H:%M', '16:00')),
                        (8, 3, 'Sports building canteen', 'Männiliiva 7, S01 building', strftime('%H:%M', '11:00'), strftime('%H:%M', '20:00'))
                        """)

    conn.execute("""INSERT INTO CANTEEN (ID, ProviderID, NAME, LOCATION, time_open, time_closed)
                 VALUES (9, 4, 'bitStop KOHVIK', 'IT College, Raja 4c', strftime('%H:%M', '09:00'), strftime('%H:%M', '16:00')) 
                        """)

    conn.commit()
    print("Records created successfully")


def select_records():
    """Fetch and display records from the CANTEEN table. Fetch and display records from the CANTEEN and PROVIDER table."""
    cursor = conn.execute\
        ("SELECT ID, NAME, LOCATION, time_open, time_closed from CANTEEN where CANTEEN.time_closed >= strftime('%H:%M','16:15')")

    for row in cursor:
        print("ID = ", row[0])
        print("Name = ", row[1])
        print("Location = ", row[2])
        print("Open time = ", row[3])
        print("Closed time = ", row[4], "\n")
    print("All canteens that are open after 4:15 pm")

    cursor = conn.execute\
        ("SELECT * from CANTEEN, PROVIDER where CANTEEN.ProviderID = PROVIDER.ID and PROVIDER.ProviderName = 'Rahva Toit'")

    for row in cursor:
        print("CanteenID = ", row[0])
        print("ProviderID = ", row[1])
        print("Canteen Name = ", row[2])
        print("Location = ", row[3])
        print("Open time = ", row[4])
        print("Closed time = ", row[5], "\n")
    print("All canteens with a provider Rahva Toit")


def delete_records():
    """Delete record in Canteen where ID = 4."""
    conn.execute("DELETE from CANTEEN where ID = 4;")
    conn.commit()
    print("Removing the dining room under index 4")


def close_connection():
    """Close connection."""
    conn.close()
    print("Connection closed")


if __name__ == "__main__":
    open_database()
    create_table()
    create_records()
    select_records()
    delete_records()
    close_connection()
