

# Напишите функцию, которая принимает строку из одного или нескольких слов и возвращает ту же строку,
# но с перевернутыми словами из пяти или более букв (точно так же, как имя этого Ката).
# Переданные строки будут состоять только из букв и пробелов.
# Пробелы будут добавлены только в том случае, если присутствует более одного слова.

sentence = "This ecnetnes is a ecnetnes"

def my_spin_words(sentence):

    new_sentence = ""
    index = 0
    listOfWords = sentence.split()

    for x in listOfWords:
        index += 1
        if len(x) >= 5:
            y = x[::-1]
            new_sentence += y
            if index != len(listOfWords):
                new_sentence += " "

        else:
            new_sentence += x
            if index != len(listOfWords):
                new_sentence += " "

    return new_sentence

def best_spin_words(sentence):
    # Your code goes here
    return " ".join([x[::-1] if len(x) >= 5 else x for x in sentence.split(" ")])

print(best_spin_words(sentence))