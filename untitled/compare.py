import math
# Даны два массива a и b напишите функцию comp(a, b)(или compSame(a, b)),
# которая проверяет, имеют ли два массива «одинаковые» элементы с одинаковой кратностью .
# «Одинаковый» здесь означает, что элементы в b- это элементы в a квадрате, независимо от порядка.

# Do not work

array1 = [121, 144, 19, 161, 19, 144, 19, 11]
array2 = [11 * 11, 121 * 121, 144 * 144, 190 * 190, 161 * 161, 19 * 19, 144 * 144, 19 * 19]

array3 = []

def comp(array1, array2):
    col = 0
    for y in array2:
        array3.append(math.sqrt(y))
    for x in array3:
        if x in array1:
            col += 1
            array3.remove(x)
    if col == len(array3):
        return True
    else:
        return False

print(comp(array1,array2))