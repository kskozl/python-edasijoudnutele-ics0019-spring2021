# Завершите решение, чтобы оно возвращало истину,
# если первый переданный аргумент (строка) заканчивается вторым аргументом (также строкой).

string = 'abc'
ending = 'bc'

def my_solution(string, ending):
    return ending == string[len(string) - len(ending):]

print(my_solution(string, ending))


def best_solution(string, ending):
    return string.endswith(ending)

print(best_solution(string, ending))